#encoding=utf-8
require "zlib"
def load_data(filename)
File.open(filename, "rb") { |f|
    obj = Marshal.load(f)
}
end
 
scripts = load_data("../Zombie_Project/Data/Scripts.rvdata2")

orderFile = File.open("order.txt", "w") 
scripts.each{|spriteObj|
	orderFile.puts "#{spriteObj[1]}".encode("utf-8", "utf-8")
}
orderFile.close

scripts.each{|spriteObj|
	f = File.open("../src/#{spriteObj[1]}.rb", "w")
	spriteObj[3] = Zlib::Inflate.inflate(spriteObj[2]).gsub("\r\n", "\n")
	p spriteObj[1]
	#f.puts "#" + spriteObj[1]
	f.puts spriteObj[3]
	f.close
}